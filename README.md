# LynxChanAddon-Webring

This addon allows multiple imageboards to collaborate by making each other's
boards visible on their boards list.

## The Webring Schema

```js
{
    "name": "Imageboard", // The imageboard's name.
    "url": "https://image.board", // The imageboard's base URL.
    "endpoint": "https://image.board/webring.json", // The current endpoint.

    // An array of logos you want to be shown for other imageboards' users
    // on the webring. Should be a .png or .gif with a maximum size of 300KB.
    "logo": [
        "https://image.board/my_logo.png"
    ],

    "following": [ // A list of imageboards that this imageboard is directly following.
        "https://another.imageboard/webring.json",
        "https://yet.another.ib/webring.json"
    ],
    "known": [
        // A list of imageboards this imageboard pairs with, but doesn't necessarily
        // follow directly.
        "https://another.imageboard/webring.json",
        "https://yet.another.ib/webring.json",
        "https://some.other.board/webring.json"
    ],
    "blacklist": [ // A list of domains that this imageboard will not pair with.
        "bad.imageboard",
        "illegal.ib"
    ],

    "boards": [
        {
            "uri": "a", // The URI of the board.
            "title": "Anime & Manga", // The board name/title.
            "subtitle": "Discussion about japanese shit", // The board subtitle/description. Optional.
            "path": "https://image.board/a/", // The absolute path for the board.

            "postsPerHour": 23, // The amount of posts made in the last hour. Optional.
            "totalPosts": 1000, // The total amount of posts on the board. Optional.
            "uniqueUsers": 20 // The total amount of unique ISPs as reported by the imageboard software. Optional.

            "tags": ["a", "b", "c"], // An array of string tags for the board.
            "lastPostTimestamp": "1970-01-01T00:00:00Z" // An ISO 8601 specifying the date and time of the newest post on the board.
        }
        // ...
    ]
}
```

## Installation for LynxChan

1. Clone this repository to `src/be/addons/webring`
2. Configure the `SITE_URL`, `WEBRING_SITES` and `SITE_LOGOS` variables at the top of `index.js`
3. Configure `blacklist.txt` if you want to blacklist imageboards
4. Add `webring` to the addons list on LynxChan global settings
5. Restart LynxChan

## License

This addon is licensed under the GNU General Public License Version 3.
&copy; The Collective of Individuals 2019.
